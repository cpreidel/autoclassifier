from flask import Flask, request, jsonify
from main_json import process_files  # Import the function from your script
from flask_cors import cross_origin
import tempfile
import os

app = Flask(__name__)

@app.route('/upload_files', methods=['POST'])
@cross_origin()
def upload_files():
    try:
        if 'ifc_file' in request.files and 'ids_file' in request.files:
            ifc_file = request.files['ifc_file']
            print(ifc_file)
            ids_file = request.files['ids_file']
            print(ids_file)

        if ifc_file.filename.endswith('.ifc') and ids_file.filename.endswith('.ids'):
            # Process the uploaded files as needed (e.g., save to a directory)
            # ifc_file.save('uploads/' + ifc_file.filename)
            # ids_file.save('uploads/' + ids_file.filename)
            result = None 
            user_home_directory = os.path.join(os.path.expanduser("~"), "my_temp_files")
            os.makedirs(user_home_directory, exist_ok=True)  # This will create the directory if it doesn't exist

            with tempfile.NamedTemporaryFile(suffix='.ifc', dir=user_home_directory, delete=False) as tmp_ifc, \
                tempfile.NamedTemporaryFile(suffix='.ids', dir=user_home_directory, delete=False) as tmp_ids:
                    
                tmp_ifc.write(ifc_file.read())
                tmp_ids.write(ids_file.read())

            result = process_files(tmp_ifc.name, tmp_ids.name)
            # print(result)
            # Return the result along with the JavaScript code to store data in localStorage
            return result
        
    except Exception as e:
        print(e)
        return jsonify({'error': f'An error occurred: {str(e)}'}), 500

    return jsonify({'error': 'Invalid file types or missing files'}), 400
    
if __name__ == "__main__":
    app.run(debug=True, port=5001)
    # app.run(debug=True)


