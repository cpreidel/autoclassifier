from ifctester import ids
import ifcopenshell
from ifctester.facet import Classification

def get_name(obj):
    """Retrieve the name of an object, or its string representation if name is absent."""
    return getattr(obj, "name", str(obj))

# Paths
ifc_path = r"ifc\Solibri Building.ifc"
# ids_path= r"ids\ids_classification.ids"
ids_path= r"ids\ids_classification.ids"

# Opening the IDS and IFC files
my_ids = ids.open(ids_path)
ifc_file = ifcopenshell.open(ifc_path)

results = {}

# Processing each specification in the IDS
for spec in my_ids.specifications:
    spec_name = get_name(spec)

    # Filtering elements for the current specification
    elements = None
    for facet in spec.applicability:
        elements = facet.filter(ifc_file, elements)
    
    guids = [ele.GlobalId for ele in elements]

    # Storing elements and classifications for the current specification
    spec_data = {
        "applicabilities": guids,
        "classifications": [
            {
                "value": facet.value,
                "system": facet.system,
                "uri": facet.uri
            }
            for facet in spec.requirements if isinstance(facet, Classification)
        ]
    }
    results[spec_name] = spec_data

# Classifying in IFC
for specifcation in results:
    for classification in results[specifcation]["classifications"]:
        # Creating new classification and reference entities
        classification_entity = ifc_file.create_entity(
            "IfcClassification",
            Name=classification["system"]
        )

        classification_reference = ifc_file.create_entity(
            "IfcClassificationReference",
            Name=classification["value"],
            ReferencedSource=classification_entity,
            ItemReference=classification["value"],
            Location=classification["uri"]
        )

        components = [ifc_file.by_guid(ele) for ele in results[specifcation]["applicabilities"]]

        ifc_file.create_entity(
            "IfcRelAssociatesClassification", 
            GlobalId=ifcopenshell.guid.new(), 
            RelatingClassification=classification_reference, 
            RelatedObjects=components
        )

# Writing to a new IFC file
ifc_file.write(ifc_path + "_classified.ifc")
print("IFC file written")
