const fetch = require('node-fetch'); // For making HTTP requests
const FormData = require('form-data'); // For creating form data

const endpoint = 'http://127.0.0.1:5000/upload_files'; // Replace with your server endpoint
const ifcFilePath = 'uploads\SmallTestModel.ifc'; // Replace with the path to your IFC file
const idsFilePath = 'uploads\ids_classification.ids'; // Replace with the path to your IDS file

async function testUpload() {
  try {
    // Create a FormData object and append the files
    const formData = new FormData();
    formData.append('ifc_file', require('fs').createReadStream(ifcFilePath));
    formData.append('ids_file', require('fs').createReadStream(idsFilePath));

    // Make a POST request to the server
    const response = await fetch(endpoint, {
      method: 'POST',
      body: formData,
    });

    if (response.ok) {
      const contentType = response.headers.get('Content-Type');
      if (contentType && contentType.includes('application/json')) {
        try {
          const responseData = await response.json();
          console.log('Response Data:', responseData);
          // Process the data here
        } catch (error) {
          console.log('Error parsing JSON response:', error);
          // Handle JSON parsing error
        }
      } else {
        console.log('Non-JSON response:', contentType);
        // Handle non-JSON response here
      }
    } else {
      console.error('HTTP error:', response.status);
      // Handle HTTP error response here
    }
  } catch (error) {
    console.error('Error during fetch:', error);
  }
}

// Call the testUpload function
testUpload();
