# autoClassifier

A brief description of what this project does and who it's for.

## Prerequisites

Ensure you have the following installed on your local machine:

- Node.js (v14.x or later)
- npm (v6.x or later)
- Python (v3.8 or later)

## Getting Started

Clone the repository to your local machine:

```bash
git clone https://github.com/your-username/autoClassifier.git
cd autoClassifier
```

## Installation
Install the necessary npm packages:

```bash
npm install
```

## Build the project:

```bash
npm run build
```

## Backend Setup
Before running the server, ensure the Python backend is up and running:

Navigate to the backend directory:
```bash
cd backend
```

Run the Python backend script:

```bash
python backend.py
```

Make sure there are no errors in the console and the backend server is running successfully.

## Running the Server
Once the Python backend is up and running, you can start the local server:

```bash
npm start
```

Your application should now be running at http://localhost:5000.

Alternatively, if you are using Visual Studio Code, you can use the Live Server extension to run your local server:

* Install the Live Server extension from the VS Code marketplace.
* Right-click on the index.html file and select Open with Live Server.
* Your application should now be running at http://127.0.0.1:5500 or a similar address, and will automatically reload if you make changes to the files.

## Further Help
For further help or issues, feel free to contact the maintainers or open an issue on GitHub.

## Contributing
Contributions are welcome! Please read the contributing guidelines to get started.