import requests

url = "http://localhost:5000/upload_files"
data = {
    "ifc_file": "ifc\\SmallTestModel.ifc",
    "ids_file": "ids\\ids_classification.ids"
}

response = requests.post(url, json=data)

print(response.status_code)
print(response.text)