import json
from ifctester import ids, reporter
from ifctester.facet import Classification

import ifcopenshell.util.classification
from tempfile import SpooledTemporaryFile


def get_name(obj):
    """Retrieve the name of an object, or its string representation if name is absent."""
    return getattr(obj, "name", str(obj))


def extract_data_from_ifc(ifc_path, ids_path):
    my_ids = ids.open(ids_path)
    ifc_file = ifcopenshell.open(ifc_path)

    results = {}

    for spec in my_ids.specifications:
        spec_name = get_name(spec)

        elements = None
        for facet in spec.applicability:
            elements = facet.filter(ifc_file, elements)

        applicabilities_set = {ele.GlobalId for ele in elements}

        for facet in spec.requirements:
            if isinstance(facet, Classification):
                system = facet.system

                item = {
                    "value": facet.value,
                    "uri": facet.uri,
                    "applicabilities": list(applicabilities_set)
                }

                results.setdefault(system, []).append(item)

    return results


def process_files(ifc_path, ids_path):
    return extract_data_from_ifc(ifc_path, ids_path)


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description='Process IFC and IDS file paths.')
    parser.add_argument('ifc_path', type=str, help='Path to the IFC file')
    parser.add_argument('ids_path', type=str, help='Path to the IDS file')

    args = parser.parse_args()

    results = process_files(args.ifc_path, args.ids_path)
    # print(results)
