<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<ids:ids xmlns:ids="http://standards.buildingsmart.org/IDS" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://standards.buildingsmart.org/IDS ids.xsd">
	<ids:info>
		<ids:title>Classification Example</ids:title>
	</ids:info>
	<ids:specifications>
		<ids:specification ifcVersion="IFC2X3 IFC4" name="Tragende Außenwände" description="Klassifikation" maxOccurs="unbounded">
			<ids:applicability>
				<ids:entity>
					<ids:name>
						<xs:restriction>
							<xs:annotation>
								<xs:documentation>This goes for either 'IFCWALL' or 'IFCWALLSTANDARDCASE'</xs:documentation>
							</xs:annotation>
							<xs:pattern value="IFCWALL|IFCWALLSTANDARDCASE"/>
						</xs:restriction>
					</ids:name>
				</ids:entity>
				<ids:property datatype="IfcBoolean">
					<ids:propertySet>
						<ids:simpleValue>Pset_WallCommon</ids:simpleValue>
					</ids:propertySet>
					<ids:name>
						<ids:simpleValue>IsExternal</ids:simpleValue>
					</ids:name>
					<ids:value>
						<ids:simpleValue>TRUE</ids:simpleValue>
					</ids:value>
				</ids:property>
				<ids:property datatype="IfcBoolean">
					<ids:propertySet>
						<ids:simpleValue>Pset_WallCommon</ids:simpleValue>
					</ids:propertySet>
					<ids:name>
						<ids:simpleValue>LoadBearing</ids:simpleValue>
					</ids:name>
					<ids:value>
						<ids:simpleValue>TRUE</ids:simpleValue>
					</ids:value>
				</ids:property>
			</ids:applicability>
			<ids:requirements>
			    <ids:classification uri="https://www.din-bim-cloud.de/">
					<ids:value>
						<ids:simpleValue>331 - Tragende Außenwände</ids:simpleValue>
					</ids:value>
					<ids:system>
						<ids:simpleValue>DIN276</ids:simpleValue>
					</ids:system>
				</ids:classification>
				<!-- <ids:classification>
					<ids:value>
						<ids:simpleValue>Aussenwand</ids:simpleValue>
					</ids:value>
					<ids:system>
						<ids:simpleValue>eBKP-H</ids:simpleValue>
					</ids:system>
				</ids:classification> -->
				<ids:classification uri="https://identifier.buildingsmart.org/uri/nlsfb/nlsfb2005/2.2/class/21.2">
					<ids:value>
						<ids:simpleValue>(21.2) buitenwanden; constructief</ids:simpleValue>
					</ids:value>
					<ids:system>
						<ids:simpleValue>NL-SfB 2005</ids:simpleValue>
					</ids:system>
				</ids:classification>
			</ids:requirements>
		</ids:specification>
		<ids:specification ifcVersion="IFC2X3 IFC4" name="Nichttragende Außenwände" description="Klassifikation" maxOccurs="unbounded">
			<ids:applicability>
				<ids:entity>
					<ids:name>
						<xs:restriction>
							<xs:annotation>
								<xs:documentation>This goes for either 'IFCWALL' or 'IFCWALLSTANDARDCASE'</xs:documentation>
							</xs:annotation>
							<xs:pattern value="IFCWALL|IFCWALLSTANDARDCASE"/>
						</xs:restriction>
					</ids:name>
				</ids:entity>
				<ids:property datatype="IfcBoolean">
					<ids:propertySet>
						<ids:simpleValue>Pset_WallCommon</ids:simpleValue>
					</ids:propertySet>
					<ids:name>
						<ids:simpleValue>IsExternal</ids:simpleValue>
					</ids:name>
					<ids:value>
						<ids:simpleValue>TRUE</ids:simpleValue>
					</ids:value>
				</ids:property>
				<ids:property datatype="IfcBoolean">
					<ids:propertySet>
						<ids:simpleValue>Pset_WallCommon</ids:simpleValue>
					</ids:propertySet>
					<ids:name>
						<ids:simpleValue>LoadBearing</ids:simpleValue>
					</ids:name>
					<ids:value>
						<xs:restriction>
							<xs:pattern value="FALSE"/>
						</xs:restriction>
						<!-- <ids:simpleValue>FALSE</ids:simpleValue> -->
					</ids:value>
					<!-- <ids:value>
						<ids:simpleValue>FALSE</ids:simpleValue>
					</ids:value> -->
				</ids:property>
			</ids:applicability>
			<ids:requirements>
			    <ids:classification uri="https://www.din-bim-cloud.de/">
					<ids:value>
						<ids:simpleValue>332 - Nichttragende Außenwände</ids:simpleValue>
					</ids:value>
					<ids:system>
						<ids:simpleValue>DIN276</ids:simpleValue>
					</ids:system>
				</ids:classification>
				<!-- <ids:classification>
					<ids:value>
						<ids:simpleValue>Aussenwand</ids:simpleValue>
					</ids:value>
					<ids:system>
						<ids:simpleValue>eBKP-H</ids:simpleValue>
					</ids:system>
				</ids:classification> -->
				<ids:classification uri="https://identifier.buildingsmart.org/uri/nlsfb/nlsfb2005/2.2/class/21.1">
					<ids:value>
						<ids:simpleValue>(21.1) buitenwanden; niet constructief</ids:simpleValue>
					</ids:value>
					<ids:system>
						<ids:simpleValue>NL-SfB 2005</ids:simpleValue>
					</ids:system>
				</ids:classification>
			</ids:requirements>
		</ids:specification>
		<ids:specification ifcVersion="IFC2X3 IFC4" name="Innenwände" description="Klassifikation" maxOccurs="unbounded">
			<ids:applicability>
				<ids:entity>
					<ids:name>
						<xs:restriction>
							<xs:annotation>
								<xs:documentation>This goes for either 'IFCWALL' or 'IFCWALLSTANDARDCASE'</xs:documentation>
							</xs:annotation>
							<xs:pattern value="IFCWALL|IFCWALLSTANDARDCASE"/>
						</xs:restriction>
					</ids:name>
				</ids:entity>
				<ids:property datatype="IfcBoolean">
					<ids:propertySet>
						<ids:simpleValue>Pset_WallCommon</ids:simpleValue>
					</ids:propertySet>
					<ids:name>
						<ids:simpleValue>IsExternal</ids:simpleValue>
					</ids:name>
					<ids:value>
						<xs:restriction>
							<xs:pattern value="^(FALSE)?$"/>
						</xs:restriction>
						<!-- <ids:simpleValue>FALSE</ids:simpleValue> -->
					</ids:value>
				</ids:property>
			</ids:applicability>
			<ids:requirements>
				<ids:classification>
					<ids:value>
						<ids:simpleValue>332</ids:simpleValue>
					</ids:value>
					<ids:system>
						<ids:simpleValue>DIN276</ids:simpleValue>
					</ids:system>
				</ids:classification>
				<ids:classification>
					<ids:value>
						<ids:simpleValue>Innenwand</ids:simpleValue>
					</ids:value>
					<ids:system>
						<ids:simpleValue>eBKP-H</ids:simpleValue>
					</ids:system>
				</ids:classification>
				<ids:classification uri="https://identifier.buildingsmart.org/uri/nlsfb/nlsfb2005/2.2/class/22.1">
					<ids:value>
						<ids:simpleValue>binnenwanden</ids:simpleValue>
					</ids:value>
					<ids:system>
						<ids:simpleValue>NL-SfB 2005</ids:simpleValue>
					</ids:system>
				</ids:classification>
			</ids:requirements>
		</ids:specification>
		<ids:specification ifcVersion="IFC2X3 IFC4" name="Fenster" description="Klassifikation" maxOccurs="unbounded">
			<ids:applicability>
				<ids:entity>
					<ids:name>
                        <ids:simpleValue>IFCWINDOW</ids:simpleValue>
                    </ids:name>
				</ids:entity>
			</ids:applicability>
			<ids:requirements>
				<ids:classification>
					<ids:value>
						<ids:simpleValue>334</ids:simpleValue>
					</ids:value>
					<ids:system>
						<ids:simpleValue>DIN276</ids:simpleValue>
					</ids:system>
				</ids:classification>
				<ids:classification>
					<ids:value>
						<ids:simpleValue>Fenster</ids:simpleValue>
					</ids:value>
					<ids:system>
						<ids:simpleValue>eBKP-H</ids:simpleValue>
					</ids:system>
				</ids:classification>
				<ids:classification uri="https://identifier.buildingsmart.org/uri/nlsfb/nlsfb2005/2.2/class/55.11">
					<ids:value>
						<ids:simpleValue>koude-opwekking</ids:simpleValue>
					</ids:value>
					<ids:system>
						<ids:simpleValue>NL-SfB 2005</ids:simpleValue>
					</ids:system>
				</ids:classification>
			</ids:requirements>
		</ids:specification>
		<ids:specification ifcVersion="IFC2X3 IFC4" name="Stütze" description="Klassifikation" maxOccurs="unbounded">
			<ids:applicability>
				<ids:entity>
					<ids:name>
                        <ids:simpleValue>IFCCOLUMN</ids:simpleValue>
                    </ids:name>
				</ids:entity>
			</ids:applicability>
			<ids:requirements>
				<ids:classification uri="https://google.com">
					<ids:value>
						<ids:simpleValue>Klaus</ids:simpleValue>
					</ids:value>
					<ids:system>
						<ids:simpleValue>Jakob</ids:simpleValue>
					</ids:system>
				</ids:classification>
			</ids:requirements>
		</ids:specification>
	</ids:specifications>
</ids:ids>