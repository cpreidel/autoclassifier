// copyFiles.js (for ES module)
import fs from 'fs';

const filesToCopy = [
  'node_modules/web-ifc/web-ifc.wasm',
  'node_modules/web-ifc/web-ifc-mt.wasm',
  'node_modules/web-ifc-three/IFCWorker.js',
  'node_modules/web-ifc/web-ifc-mt.worker.js',
  'node_modules/web-ifc-three/IFCWorker.js.map'
];

filesToCopy.forEach(async file => {
  const data = await fs.promises.readFile(file);
  await fs.promises.writeFile(`./${file.split('/').pop()}`, data);
});
