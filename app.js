import { Color } from "three";
import { IfcViewerAPI } from "web-ifc-viewer";

let model;
let ifcProject;

const container = document.getElementById("viewer-container");
const viewer = new IfcViewerAPI({
  container,
  backgroundColor: new Color(0xffffff),
});
viewer.axes.setAxes();
viewer.grid.setGrid();

const ifcFileInput = document.getElementById('ifc-file-input');
const idsFileInput = document.getElementById('ids-file-input'); // Add an IDS file input element

viewer.clipper.active = true;

window.onkeydown = (event) => {
  if (event.code === "KeyP") {
    viewer.clipper.createPlane();
  } else if (event.code === "KeyO") {
    viewer.clipper.deletePlane();
  }
};

async function loadIfc(url) {
  // Remove the current model from the viewer ... only one model at once allowed 
  if(model)
  {
    viewer.IFC.removeIfcModel(model.modelID);
    viewer.IFC.selector.unHighlightIfcItems(model.modelID);
    viewer.IFC.selector.unpickIfcItems(model.modelID);
  }

  // Load the new model
  model = await viewer.IFC.loadIfcUrl(url);
  // viewer.shadowDropper.renderShadow();
  ifcProject = await globalIDToexpressID(viewer, model.modelID);
}

// Event listener for selecting IDS files
idsFileInput.addEventListener('change', async function() {
  const idsFile = idsFileInput.files[0];
  if (idsFile && idsFile.name.endsWith('.ids')) {
      updateDropZoneWithFileName(idsDropZone, idsFile);
  }
  checkFilesAndUpload(); // Call the function to check both files and possibly upload
});

async function uploadFiles(ifcFile, idsFile) {

  const classificationsList = document.getElementById("classifications-list");
  const spinner = document.getElementById("loading-spinner");

  // Hide the list and show the spinner
  classificationsList.innerHTML = '';
  spinner.style.display = 'block';

  // Check if both files are provided
  if (ifcFile && idsFile && ifcFile.name.endsWith('.ifc') && idsFile.name.endsWith('.ids')) {

    const formData = new FormData();
    formData.append('ifc_file', ifcFile);
    formData.append('ids_file', idsFile);

    const endpoint = 'http://127.0.0.1:5001/upload_files';

    try {
      const response = await fetch(endpoint, {
        method: 'POST',
        body: formData
      });

      // If the response is ok, process it
      if (response.ok) {
        const responseData = await response.json();

        // Use the responseData here
        // console.log(responseData.toString());
        // Iterate through all top-level entries
        populateTreeView(responseData);
        // for (let key in responseData) {
        //   populateTreeView(responseData[key], key);
        // } 
        // return responseData;

      } else {
        const errorData = await response.json();
        console.error('Server error:', errorData.error);
        return null;
      }

    } catch (error) {
      console.error("Network or other error:", error);
      return null;
    }
    
  } else {
    alert('Invalid file type or missing IFC/IDS file. Please select both an IFC and an IDS file.');
    return null;
  }
  // At the end, hide the spinner
  spinner.style.display = 'none';
}

function populateTreeView(data) {
  const classificationsList = document.getElementById("classifications-list");

    // Clear existing content
    classificationsList.innerHTML = '';

  // Render data to HTML treeview
  for (let key in data) {
    // Create key as major header
    const keyHeader = document.createElement("h3");
    keyHeader.textContent = key;
    classificationsList.appendChild(keyHeader);

    // Display its values with URIs
    const ul = document.createElement('ul');

    data[key].forEach((item) => {
      const li = document.createElement("li");

      const span = document.createElement('span');
      span.textContent = `${item.value} ` + '(' + item.applicabilities.length + ') ';
      // span.style.backgroundColor = '#555';
      // span.Color = Color.globalIDToexpressID; 
      span.onclick = () => showApplicabilityIds(item.applicabilities);

      li.appendChild(span);
      
      const a = document.createElement('a');

      if (item.uri) {
        a.href = item.uri;
        a.target = "_blank";
        a.textContent = "URI"; // change this line
        li.appendChild(a);
    }
      
      ul.appendChild(li);
    });

    classificationsList.appendChild(ul);
  }
}

async function showApplicabilityIds(applicabilities) {
  if(ifcProject)
  {
    const componentIds = []
    for (var i = 0; i < applicabilities.length; i++) {
      componentIds.push(ifcProject[applicabilities[i]])
    }

    if(componentIds.length >0)
    {
      viewer.IFC.selector.unHighlightIfcItems(model.modelID);
      viewer.IFC.selector.unpickIfcItems(model.modelID);
      // viewer.shadowDropper.deleteShadow();
      viewer.IFC.selector.highlightIfcItemsByID(model.modelID, componentIds);
      viewer.IFC.selector.pickIfcItemsByID(model.modelID, componentIds);
    }
    else 
    {
      // viewer.shadowDropper.renderShadow();
      viewer.IFC.selector.unHighlightIfcItems(model.modelID);
      viewer.IFC.selector.unpickIfcItems(model.modelID);
    }
      
  }
}

export const globalIDToexpressID = async (viewer, modelID) => {
  const ifcProject = await viewer.IFC.getSpatialStructure(modelID, true);
  console.log('ifcProject', ifcProject);
  let o = {};

  function recursive(data) {
       return data.map(({ expressID, GlobalId, children}) => {
            o[GlobalId.value] = expressID;
            if (children.length) recursive(children);
            return o;
       });
  }
  return recursive(ifcProject.children)[0];
};

// Get elements for drop zones
const ifcDropZone = document.getElementById('drop-zone-ifc');
const idsDropZone = document.getElementById('drop-zone-ids');

// Event handlers
async function handleFileSelect(event, fileInput) {
    const files = event.dataTransfer.files; // Get the file from the event

    // Check if a file was dragged and dropped
    if (files.length) {
        fileInput.files = files;
        fileInput.dispatchEvent(new Event('change')); // Trigger the change event
    }
}

ifcDropZone.addEventListener('dragover', (e) => {
    e.preventDefault();
    e.stopPropagation();
    e.currentTarget.classList.add('dragover');
}, false);

idsDropZone.addEventListener('dragover', (e) => {
    e.preventDefault();
    e.stopPropagation();
    e.currentTarget.classList.add('dragover');
}, false);

ifcDropZone.addEventListener('dragleave', (e) => {
    e.currentTarget.classList.remove('dragover');
}, false);

idsDropZone.addEventListener('dragleave', (e) => {
    e.currentTarget.classList.remove('dragover');
}, false);

ifcDropZone.addEventListener('drop', (e) => {
    e.preventDefault();
    e.stopPropagation();
    e.currentTarget.classList.remove('dragover');
    handleFileSelect(e, ifcFileInput);
}, false);

idsDropZone.addEventListener('drop', (e) => {
    e.preventDefault();
    e.stopPropagation();
    e.currentTarget.classList.remove('dragover');
    handleFileSelect(e, idsFileInput);
}, false);

ifcDropZone.addEventListener('click', () => {
  ifcFileInput.click();
}, false);

idsDropZone.addEventListener('click', () => {
    idsFileInput.click();
}, false);

async function updateDropZoneWithFileName(dropZone, file) {
  const fileNameEl = dropZone.querySelector('.file-name');
  // const removeFileEl = dropZone.querySelector('.remove-file');

  fileNameEl.textContent = file.name;

  // removeFileEl.style.display = 'inline-block';

  // removeFileEl.onclick = function() {
  //     if (dropZone === ifcDropZone) {
  //         ifcFileInput.value = ""; // Clear file input
  //     } else if (dropZone === idsDropZone) {
  //         idsFileInput.value = "";
  //     }
  //     fileNameEl.textContent = "Drop File or Click to Upload";
  //     removeFileEl.style.display = 'none';
  //     // uploadFiles(ifcFileInput.files[0], idsFileInput.files[0]); // update after removing
  // };
}

// Updated 'change' event listener for IFC:
ifcFileInput.addEventListener('change', function() {
  const file = ifcFileInput.files[0];
  if (file && file.name.endsWith('.ifc')) {
      const blobUrl = URL.createObjectURL(file);
      console.log("IFC file uploaded:", file);
      loadIfc(blobUrl);
      updateDropZoneWithFileName(ifcDropZone, file);
  }
  checkFilesAndUpload(); // Call the function to check both files and possibly upload
});

// Updated 'change' event listener for IDS:
idsFileInput.addEventListener('change', async function() {
  const idsFile = idsFileInput.files[0];
  const ifcFile = ifcFileInput.files[0];

  if (idsFile && idsFile.name.endsWith('.ids')) {
      updateDropZoneWithFileName(idsDropZone, idsFile);
  }

  if (ifcFile && ifcFile.name.endsWith('.ifc') && idsFile && idsFile.name.endsWith('.ids')) {
      uploadFiles(ifcFile, idsFile);
  }
});

function checkFilesAndUpload() {
  const idsFile = idsFileInput.files[0];
  const ifcFile = ifcFileInput.files[0];
  
  if (ifcFile && ifcFile.name.endsWith('.ifc') && idsFile && idsFile.name.endsWith('.ids')) {
      uploadFiles(ifcFile, idsFile);
  }
}

// const modeToggleCheckbox = document.getElementById('mode-toggle-checkbox');
// const modeLabel = document.getElementById('mode-label');

// modeToggleCheckbox.addEventListener('change', function() {
//     if (modeToggleCheckbox.checked) {
//         modeLabel.textContent = 'Highlighting';
//         // Logic for Highlighting mode
//     } else {
//         modeLabel.textContent = 'Picking';
//         // Logic for Picking mode
//     }
// });

document.addEventListener('DOMContentLoaded', function() {
  const toggleTopBtn = document.getElementById('toggle-top-sidebar');
  const topSidebar = document.getElementById('top-sidebar');
  const body = document.querySelector('body');

  const button1 = document.getElementById('button1');
  const button2 = document.getElementById('button2');

  button1.addEventListener('click', function() {
      // Führen Sie hier die Funktion für Button 1 aus
      handleButton1Click();
  });

  button2.addEventListener('click', function() {
      // Führen Sie hier die Funktion für Button 2 aus
      handleButton2Click();
  });


  toggleTopBtn.addEventListener('click', function() {
      topSidebar.style.display = topSidebar.style.display === 'none' ? 'block' : 'none';
      body.classList.toggle('has-top-sidebar', topSidebar.style.display === 'block');
  });
});

function handleButton1Click() {
  // Hier kommt Ihre Logik für Button 1
  console.log('Button 1 wurde geklickt');
}

function handleButton2Click() {
  // Hier kommt Ihre Logik für Button 2
  console.log('Button 2 wurde geklickt');
}

